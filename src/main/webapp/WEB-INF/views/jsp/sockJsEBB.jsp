<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>puppy kitty ^^</title>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/sockjs-client/0.3.4/sockjs.min.js"></script>
<script src="http://localhost:8080/resources/core/js/vertxbus-2.1.js"></script>
<script type="text/javascript">

  var eb = null;
  var subscribed = false;
  //var eb = new SockJS("http://localhost:13579/mySockJS2");
  //eb = new vertx.EventBus('http://localhost:13579/mySockJS2');
  
  var handler = function(msg, replyTo) {
    console.log('message', msg);
    $('#message-box').append(msg+'\n');
  };
  
  $(document).ready(function() {

    open();

    
    $('#subscribe-btn').click(function() {
    	subscribe($('#adress').val());
    });
    $('#desubscribe-btn').click(function() {
    	desubscribe($('#adress').val());
    });
    $('#send-btn').click(function() {
    	publish($('#adress').val(), $('#message').val());
    });
    $('#message').keypress(function(e) {
      if (e.which == 13) {
    	  publish($('#adress').val(), $('#message').val());
      }
    });
    $('#close-btn').click(function() {
    	close();
    });
    $('#open-btn').click(function() {
    	window.location.href ="/sockJsEBB.do"
    });
    
    
    
    
    
  });
  
  function open() {

	  
    if (!eb) {
       eb = new vertx.EventBus('http://localhost:13579/mySockJS2');   
       eb.onopen = function() {
        console.log('open');
        $('#status-label').html('Status: connected');

      };
      eb.onheartbeat = function(){
    	  console.log('onheartbeat');
      }
      eb.onclose = function() {
        console.log('close');
        $('#status-label').html('Status: Not connected');
      };
    }

    
  }
  
  function close() {
    if (eb) {    	
      eb.close();
    }
    
    $('#status-label').html('Status: Not connected');
  }
  
  function subscribe(address) {

	if (eb && !subscribed) {

      eb.registerHandler(address, handler);
      subscribed = true;
      
      $('#status-label').html('Status: connected , Subscribe: '+address);
    }
  }
  
  function desubscribe(address) {
	  
    if (eb && subscribed) {
      eb.unregisterHandler(address, handler);
      subscribed = false;
      $('#status-label').html('Status: connected');
    }
  }
  
  function publish(address, message) {
	$('#message').val('');
	$('#message').focus();
	
	if (!subscribed) {
	  alert('Subscribed이 필요합니다.!');
	  return;
	}
	if (eb && message.length>0) {
	  eb.publish(address, message);
	}
  }
</script>
</head>
<body style="background: url('/resources/core/images/cat.jpg');">
  <div id="status-label" >연결상태 : .. </div>
  <hr/>
  <label>채팅룸 : </label><input type="text" id="adress" value="room1">
  <input type="button" id="subscribe-btn" value="대화방 입장">
  <input type="button" id="desubscribe-btn" value="대화방 퇴장">
  <hr/>
  <textarea id="message-box" rows="20" cols="55"></textarea><br/>
  <label>메세지 : </label><input type="text" id="message" value="" size="40">
  <input type="button" id="send-btn" value="전송">
  <br>
   <input type="button" id="open-btn" value="연결">
   <input type="button" id="close-btn" value="연결 종료">
</body>
</html>