<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
<script src="/resources/core//js/jquery-1.10.2.min.js"></script>
<script src="/resources/core//js/socket.io.js"></script>
<script>
	$(document).ready(function() {
		//var socket = io.connect("http://localhost:12345");
		var socket = io.connect("http://192.168.0.5:12345");
		socket.on('response', function(msg){
			console.log("receive message :: " + msg.msg);
			
			$("#response").val($("#response").val() + "\n" +  msg.msg);
		});
		
		$("#sendBtn").bind("click", function() {
			var msg = $("input[name=chat]").val();
			socket.emit('msg', {msg:msg});
		});
		
	});
</script>
</head>
<body>
	<h1>푸시 서비스 테스트 </h1>
	<input type="text" name="chat" />
	<input type="button" value="send" id="sendBtn" />
	<br><br>
	도착메세지 :
	<br>
	<textarea id="response" name="response" rows="30" cols="30"></textarea>
</body>
</html>