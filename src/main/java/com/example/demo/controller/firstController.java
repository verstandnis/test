package com.example.demo.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class firstController {

    @GetMapping("/hi")
    public String niceToMeetYou(Model model){
        model.addAttribute("username","su");
        return "greetings";
    }
    @GetMapping("/bye")
    public String seeya(Model model){
        model.addAttribute("username","su");
        return "bye";
    }
}
