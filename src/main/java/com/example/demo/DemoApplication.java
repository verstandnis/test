package com.example.demo;

import io.vertx.core.Vertx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import javax.annotation.PostConstruct;

@ComponentScan({"com.example.demo","com.example.demo.hellowolrd.service"})
@SpringBootApplication
public class DemoApplication {


	@Autowired
	private SockJSEbbChatServerVerticle svs;


	public static void main(String[] args) {
		ApplicationContext ac = SpringApplication.run(DemoApplication.class, args);

//		for(String beanName: ac.getBeanDefinitionNames()){
//			System.out.println(beanName);
//		}
	}

	@PostConstruct
	public void deployVerticle(){
		Vertx vertx = Vertx.vertx();
		vertx.deployVerticle(svs);

	}

}
