package com.example.demo;


import java.io.File;
import java.net.URISyntaxException;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.SharedData;
import io.vertx.ext.bridge.BridgeOptions;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.ext.web.handler.sockjs.SockJSHandlerOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
public class SockJSEbbChatServerVerticle extends AbstractVerticle {

    private Logger logger;
    private HttpServer httpServer;
    private int port;
    private String origin;


    @Override
    public void start() {
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

        System.out.println("^^^^^^^^^^^^^^^^ start ^^^^^^^^^^^^^^^^^^");


        /*
        JsonObject appConfig = new JsonObject();
        httpServer = this.vertx.createHttpServer();
        port = appConfig.getInteger("port", 13579);
        origin =appConfig.getString("origin", "http://localhost");

        httpServer.requestHandler(new Handler<HttpServerRequest>() {


            @Override
            public void handle(HttpServerRequest request) {
                System.out.println("^^^^^^^^^^^^^^^^ httpServer.requestHandler ^^^^^^^^^^^^^^^^^^");
                request.response().setStatusCode(200).end("OK");
            }
        });
        */


        Router router = Router.router(vertx);
        SockJSHandlerOptions options = new SockJSHandlerOptions()
                .setHeartbeatInterval(2000);
        SockJSHandler sockJSHandler = SockJSHandler.create(vertx, options);
        router.route("/eventbus/*").handler(sockJSHandler);
        router.route().handler(staticHandler());
        router.mountSubRouter("/myapp", sockJSHandler.socketHandler(sockJSSocket -> {

            // Just echo the data back
            sockJSSocket.handler(sockJSSocket::write);

        }));
        vertx.createHttpServer()
                .requestHandler(router)
                .listen(13579);

    }
    private StaticHandler staticHandler() {
        return StaticHandler.create()
                .setCachingEnabled(false);
    }

}